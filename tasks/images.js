const fs = require('fs')
const path = require('path')

const dir = 'public/images'
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir)
}

fs.readdirSync(path.join('src', 'images')).forEach(file => {
  try {
    let filePath = path.join('src', 'images', file)
    if (fs.lstatSync(filePath).isFile()) {
      if (filePath.endsWith('.png') || filePath.endsWith('.jpg')) {
        const input = fs.readFileSync(filePath, 'binary')
        const output = `public/images/${path.basename(filePath)}`
        fs.writeFileSync(output, input, 'binary')
        console.log(`  copy ${filePath}`)
      } else if (filePath.endsWith('.ico')) {
        const input = fs.readFileSync(filePath, 'binary')
        const output = `public/${path.basename(filePath)}`
        fs.writeFileSync(output, input, 'binary')
        console.log(`  copy ${filePath}`)
      }
    }
  } catch (e) {
    console.log('', e)
    throw e
  }
})
