const uncss = require('uncss');
const fs = require('fs');

const files = ['public/index.html', 'public/asciidoc-googledocs-addon.html', 'public/asciidoc-googledocs-addon-privacy.html', 'public/weeknotes.html'],
  options = {
    ignore: ['.navbar-menu.is-active', /\.navbar-burger\.is-active.*/],
    stylesheets: ['stylesheets/main.css'],
    timeout: 1000,
    htmlroot: 'public',
    report: false,
    inject       : function(window){ window.document.querySelector('html').classList.add('no-csscalc', 'csscalc'); }
  };

uncss(files, options, function (error, output) {
  fs.writeFileSync('public/stylesheets/app.css', output, 'utf-8')
  console.log(`  uncss app.css`)
});
