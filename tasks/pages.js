const fs = require('fs')
const path = require('path')

fs.readdirSync(path.join('src', 'pages')).forEach(file => {
  try {
    let filePath = path.join('src', 'pages', file)
    if (fs.lstatSync(filePath).isFile() && filePath.endsWith('.html')) {
      let input = fs.readFileSync(filePath, 'utf-8')
      input = input.replace(/main\.css/, 'app.min.css')
      const output = `public/${path.basename(filePath)}`
      fs.writeFileSync(output, input, 'utf-8')
      console.log(`  copy ${filePath}`)
    }
  } catch (e) {
    console.log('', e)
    throw e
  }
})
